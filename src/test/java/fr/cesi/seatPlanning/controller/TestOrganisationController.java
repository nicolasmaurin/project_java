package fr.cesi.seatPlanning.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestOrganisationController {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testGetOrganisationsShouldWork() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/organisations").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testPostOrganisationShouldFailForExistingValue() throws Exception{

        String payload = "{ \"name\": \"Orga Duplicata\" }";
        mvc.perform(MockMvcRequestBuilders.post("/organisations").content(payload).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }
}
