package fr.cesi.seatPlanning.tools;

import fr.cesi.seatPlanning.FurnitureTypeEnum;
import fr.cesi.seatPlanning.exceptions.FurnitureTypeNotValidException;

import java.security.InvalidParameterException;

public class DTOValidator {
    public static void validateNonNullNonEmpty(String name, String field){
        if (field == null){
            throw new InvalidParameterException( name + " cannot be null");
        }
        if (field == ""){
            throw new InvalidParameterException( name + " cannot be empty");
        }
    }

    public static void validatePositiveInt(String name, Integer field){
        if (field <= 0){
            throw new InvalidParameterException( name + " must be > 0");
        }
    }

//    public static void validateIsInEnum(Enum<?> enumeration, String value, Exception e){
//        Integer counter = 1;
//
//        for (Enum c : enumeration.values()) {
//            System.out.println(this.type + "---->" + c.name());
//            if (c.name().equals(this.type)) {
//                break;
//            }
//            if(counter == FurnitureTypeEnum.values().length){
//                throw new FurnitureTypeNotValidException();
//            }
//            counter++;
//        }
//    }

}
