package fr.cesi.seatPlanning.controller;

import fr.cesi.seatPlanning.dto.AddFurnitureToWorkerDTO;
import fr.cesi.seatPlanning.dto.WorkerDTO;
import fr.cesi.seatPlanning.exceptions.WorkerNotFoundException;
import fr.cesi.seatPlanning.exceptions.FurnitureNotFoundException;
import fr.cesi.seatPlanning.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;
import java.util.List;

@Controller
public class WorkerController extends BaseController {

    @Autowired
    WorkerService workerService;

    @GetMapping(path = "/workers")
    public @ResponseBody
    List<WorkerDTO> getWorkerList() {
        return workerService.getAll();
    }

    @PostMapping(path = "/workers")
    public @ResponseBody
    WorkerDTO addWorker(@RequestBody WorkerDTO workerDTO) {
        return workerService.add(workerDTO);
    }

    @DeleteMapping(path="/workers")
    public @ResponseBody boolean deleteWorker(@RequestParam Integer id) throws WorkerNotFoundException {
        if (id == null) throw new InvalidParameterException("id cannot be null");
        return workerService.delete(id);
    }

    @PostMapping(path = "/workers/furnitures")
    public @ResponseBody
    WorkerDTO addFurnitureToWorker(@RequestBody AddFurnitureToWorkerDTO request) throws WorkerNotFoundException, FurnitureNotFoundException {
        request.validate();
        return workerService.addFurniture(request.getWorkerId(), request.getFurnitureId());
    }
}
