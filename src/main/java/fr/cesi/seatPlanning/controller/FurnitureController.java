package fr.cesi.seatPlanning.controller;

import fr.cesi.seatPlanning.dto.FurnitureDTO;
import fr.cesi.seatPlanning.exceptions.FurnitureNotFoundException;
import fr.cesi.seatPlanning.exceptions.FurnitureTypeNotValidException;
import fr.cesi.seatPlanning.service.FurnitureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class FurnitureController extends BaseController {

    @Autowired
    FurnitureService furnitureService;

    @GetMapping(path="/furnitures")
    public @ResponseBody
    List<FurnitureDTO> getFurnitureList(){return furnitureService.getAll();}

    @PostMapping(path="/furnitures")
    public @ResponseBody
    FurnitureDTO addFurniture(@RequestBody FurnitureDTO furnitureDTO) throws FurnitureTypeNotValidException {
        furnitureDTO.validate();
        return furnitureService.add(furnitureDTO);
    }

    @DeleteMapping(path="/furnitures")
    public @ResponseBody
    boolean addFurniture(@RequestParam Integer id) throws FurnitureNotFoundException {
        return furnitureService.delete(id);
    }
}
