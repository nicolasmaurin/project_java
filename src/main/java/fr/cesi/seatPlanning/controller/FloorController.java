package fr.cesi.seatPlanning.controller;

import fr.cesi.seatPlanning.dto.AddAreaToFloorDTO;
import fr.cesi.seatPlanning.dto.FloorDTO;
import fr.cesi.seatPlanning.exceptions.AreaNotFoundException;
import fr.cesi.seatPlanning.exceptions.FloorNotFoundException;
import fr.cesi.seatPlanning.exceptions.*;
import fr.cesi.seatPlanning.service.FloorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class FloorController extends BaseController {

    @Autowired
    FloorService floorService;

    @GetMapping(path = "/floors")
    public @ResponseBody
    List<FloorDTO> getFloorList() {
        return floorService.getAll();
    }

    @PostMapping(path = "/floors")
    public @ResponseBody
    FloorDTO addFloor(@RequestBody FloorDTO floorDTO) throws InvalidParameterException {
        floorDTO.validate();
        return floorService.add(floorDTO);
    }

    @PostMapping(path="/floors/areas")
    public @ResponseBody
    FloorDTO addAreaToFloor(@RequestBody AddAreaToFloorDTO request) throws FloorNotFoundException, AreaNotFoundException, TooBigAreaException {
        return floorService.addArea(request.getFloorId(), request.getAreaId());
    }
}
