package fr.cesi.seatPlanning.controller;

import fr.cesi.seatPlanning.dto.AddFurnitureToAreaDTO;
import fr.cesi.seatPlanning.dto.AreaDTO;
import fr.cesi.seatPlanning.exceptions.AreaNotFoundException;
import fr.cesi.seatPlanning.exceptions.FurnitureNotFoundException;
import fr.cesi.seatPlanning.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static jdk.internal.dynalink.support.Guards.isNull;

@Controller
public class AreaController extends BaseController {

    @Autowired
    AreaService areaService;

    @GetMapping(path = "/areas")
    public @ResponseBody
    List<AreaDTO> getAreaList() {
        return areaService.getAll();
    }

    @PostMapping(path = "/areas")
    public @ResponseBody
    AreaDTO addArea(@RequestBody AreaDTO areaDTO) {
        areaDTO.validate();
        return areaService.add(areaDTO);
    }

    @PostMapping(path = "/areas/furnitures")
    public @ResponseBody
    AreaDTO addFurnitureToArea(@RequestBody AddFurnitureToAreaDTO request) throws AreaNotFoundException, FurnitureNotFoundException {
        request.validate();
        return areaService.addFurniture(request.getAreaId(), request.getFurnitureId());
    }

}
