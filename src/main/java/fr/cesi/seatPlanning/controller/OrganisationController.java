package fr.cesi.seatPlanning.controller;

import fr.cesi.seatPlanning.dto.AddAreaToOrganisationDTO;
import fr.cesi.seatPlanning.dto.AddWorkerToOrganisationDTO;
import fr.cesi.seatPlanning.dto.IOrganisationDTO;
import fr.cesi.seatPlanning.dto.OrganisationDTO;
import fr.cesi.seatPlanning.exceptions.AreaNotFoundException;
import fr.cesi.seatPlanning.exceptions.OrganisationAlreadyExistingException;
import fr.cesi.seatPlanning.exceptions.OrganisationNotFoundException;
import fr.cesi.seatPlanning.exceptions.WorkerNotFoundException;
import fr.cesi.seatPlanning.service.OrganisationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class OrganisationController extends BaseController {

    @Autowired
    OrganisationService organisationService;

    @GetMapping(path="/organisations")
    public @ResponseBody
    List<IOrganisationDTO> getOrganisationsList(){
        return organisationService.getAll();
    }

    @PostMapping(path="/organisations")
    public @ResponseBody
    IOrganisationDTO addOrganisation(@RequestBody OrganisationDTO organisationDTO) throws OrganisationAlreadyExistingException {
        organisationDTO.validate();
        return organisationService.add(organisationDTO);
    }

    @PostMapping(path="/organisations/workers")
    public @ResponseBody
    IOrganisationDTO addWorkerToOrganisation(@RequestBody AddWorkerToOrganisationDTO request) throws OrganisationNotFoundException, WorkerNotFoundException {
        return organisationService.addWorker(request.getOrganisationId(), request.getWorkerId());
    }

    @PostMapping(path="/organisations/areas")
    public @ResponseBody
    IOrganisationDTO addAreaToOrganisation(@RequestBody AddAreaToOrganisationDTO request) throws OrganisationNotFoundException, AreaNotFoundException {
        return organisationService.addArea(request.getOrganisationId(), request.getAreaId());
    }

}
