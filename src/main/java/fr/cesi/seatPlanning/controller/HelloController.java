package fr.cesi.seatPlanning.controller;


import fr.cesi.seatPlanning.dto.IOrganisationDTO;
import fr.cesi.seatPlanning.dto.WorkerDTO;
import fr.cesi.seatPlanning.exceptions.OrganisationNotFoundException;
import fr.cesi.seatPlanning.exceptions.WorkerNotFoundException;
import fr.cesi.seatPlanning.service.OrganisationService;
import fr.cesi.seatPlanning.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class HelloController extends BaseController {

    @Autowired
    OrganisationService organisationService;

    @Autowired
    WorkerService workerService;


    @RequestMapping(value = "/index")
    public String index(Model model) {

        List<IOrganisationDTO> organisationsList = organisationService.getAll();
        model.addAttribute("organisations", organisationsList);
        return "index";

    }

    @RequestMapping(value = "/page/organisations")
    public String pageOrganisations(Model model) {
        List<IOrganisationDTO> organisationsList = organisationService.getAll();
        model.addAttribute("organisations", organisationsList);
        return "organisations";

    }

    @RequestMapping(value = "/page/workers")
    public String pageWorkers(@RequestParam Integer organisationId, Model model) throws OrganisationNotFoundException {
        List<WorkerDTO> workersList = workerService.getAllByOrganisationId(organisationId);
        model.addAttribute("workers", workersList);
        return "workers";

    }

    @RequestMapping(value = "/page/workers/delete")
    public String pageWorkersDelete(@RequestParam Integer worker_id, Model model) throws OrganisationNotFoundException, WorkerNotFoundException {
        workerService.delete(worker_id);
        return "workers";

    }

    @PostMapping(value = "/test")
    public String addWorkerToCompany(@RequestParam String fullName, Model model) throws OrganisationNotFoundException, WorkerNotFoundException {
        System.out.println(fullName);
        return "workers";

    }


}
