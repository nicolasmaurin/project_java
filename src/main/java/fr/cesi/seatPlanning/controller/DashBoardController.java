package fr.cesi.seatPlanning.controller;

import fr.cesi.seatPlanning.dto.OrganisationAreaOccupationDTO;
import fr.cesi.seatPlanning.exceptions.OrganisationNotFoundException;
import fr.cesi.seatPlanning.service.DashBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
public class DashBoardController extends BaseController{

    @Autowired
    DashBoardService dashBoardService;

    @GetMapping(path = "/dashboard")
    public @ResponseBody
    Map<String, OrganisationAreaOccupationDTO> getDashboard() throws OrganisationNotFoundException {
        return dashBoardService.getDashBoard();
    }


}
