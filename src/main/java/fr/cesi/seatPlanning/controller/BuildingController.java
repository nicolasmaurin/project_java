package fr.cesi.seatPlanning.controller;

import fr.cesi.seatPlanning.dto.AddFloorToBuildingDTO;
import fr.cesi.seatPlanning.dto.BuildingDTO;
import fr.cesi.seatPlanning.exceptions.BuildingNotFoundException;
import fr.cesi.seatPlanning.exceptions.FloorNotFoundException;
import fr.cesi.seatPlanning.service.BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class BuildingController extends BaseController {

    @Autowired
    BuildingService buildingService;

    @GetMapping(path = "/buildings")
    public @ResponseBody
    List<BuildingDTO> getBuildingList() {
        return buildingService.getAll();
    }

    @PostMapping(path = "/buildings")
    public @ResponseBody
    BuildingDTO addBuilding(@RequestBody BuildingDTO buildingDTO) {
        return buildingService.add(buildingDTO);
    }

    @PostMapping(path="/buildings/floors")
    public @ResponseBody
    BuildingDTO addFloorToBuilding(@RequestBody AddFloorToBuildingDTO request) throws BuildingNotFoundException, FloorNotFoundException {
        return buildingService.addFloor(request.getBuildingId(), request.getFloorId());
    }
}
