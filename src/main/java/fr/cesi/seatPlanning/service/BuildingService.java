package fr.cesi.seatPlanning.service;

import fr.cesi.seatPlanning.dto.BuildingDTO;
import fr.cesi.seatPlanning.dto.FloorDTO;
import fr.cesi.seatPlanning.exceptions.AreaNotFoundException;
import fr.cesi.seatPlanning.exceptions.BuildingNotFoundException;
import fr.cesi.seatPlanning.exceptions.FloorNotFoundException;
import fr.cesi.seatPlanning.model.Area;
import fr.cesi.seatPlanning.model.Building;
import fr.cesi.seatPlanning.model.Floor;
import fr.cesi.seatPlanning.repository.BuildingRepository;
import fr.cesi.seatPlanning.repository.FloorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BuildingService {

    @Autowired
    BuildingRepository buildingRepository;

    @Autowired
    FloorRepository floorRepository;

    public List<BuildingDTO> getAll() {
        List<Building> buildingList = buildingRepository.findAll();
        List<BuildingDTO> buildingDTOList = new ArrayList<>();
        for (Building building : buildingList) {
            buildingDTOList.add(new BuildingDTO(building));
        }
        return buildingDTOList;
    }

    public BuildingDTO add(BuildingDTO buildingDTO) {
        Building buildingToCreate = new Building(buildingDTO);
        Building buildingCreated = buildingRepository.save(buildingToCreate);
        BuildingDTO buildingReturn = new BuildingDTO(buildingCreated);
        return buildingReturn;
    }

    public BuildingDTO addFloor (Integer buildingId, Integer floorId) throws BuildingNotFoundException, FloorNotFoundException {
        Building building = buildingRepository.findById(buildingId).orElseThrow(BuildingNotFoundException::new);
        Floor floor = floorRepository.findById(floorId).orElseThrow(FloorNotFoundException::new);
        floor.setBuilding(building);
        floorRepository.save(floor);
        return new BuildingDTO(buildingRepository.findById(building.getId()).orElseThrow(BuildingNotFoundException::new));
    }

}
