package fr.cesi.seatPlanning.service;

import fr.cesi.seatPlanning.dto.FurnitureDTO;
import fr.cesi.seatPlanning.exceptions.FurnitureNotFoundException;
import fr.cesi.seatPlanning.model.Furniture;
import fr.cesi.seatPlanning.repository.AreaRepository;
import fr.cesi.seatPlanning.repository.FurnitureRepository;
import fr.cesi.seatPlanning.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FurnitureService {

    @Autowired
    FurnitureRepository furnitureRepository;
    @Autowired
    WorkerRepository workerRepository;
    @Autowired
    AreaRepository areaRepository;

    public List<FurnitureDTO> getAll() {
        List<Furniture> furnitureList = furnitureRepository.findAll();
        List<FurnitureDTO> furnitureDtoList = new ArrayList<>();
        for (Furniture furniture : furnitureList) {
            furnitureDtoList.add(new FurnitureDTO(furniture));
        }
        return furnitureDtoList;
    }

    public FurnitureDTO add(FurnitureDTO furnitureDTO) {
        Furniture furnitureToCreate = new Furniture(furnitureDTO);
        Furniture furnitureCreated = furnitureRepository.save(furnitureToCreate);
        FurnitureDTO furnitureReturn = new FurnitureDTO(furnitureCreated);
        return furnitureReturn;
    }

    public boolean delete(Integer furnitureId) {
        furnitureRepository.deleteById(furnitureId);
        try {
            furnitureRepository.findById(furnitureId).orElseThrow(FurnitureNotFoundException::new);
            return false;
        } catch (FurnitureNotFoundException e) {
            return true;
        }
    }
}
