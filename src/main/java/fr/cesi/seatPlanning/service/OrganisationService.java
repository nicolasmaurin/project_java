package fr.cesi.seatPlanning.service;

import fr.cesi.seatPlanning.dto.IOrganisationDTO;
import fr.cesi.seatPlanning.dto.OrganisationDTO;
import fr.cesi.seatPlanning.exceptions.AreaNotFoundException;
import fr.cesi.seatPlanning.exceptions.OrganisationAlreadyExistingException;
import fr.cesi.seatPlanning.exceptions.OrganisationNotFoundException;
import fr.cesi.seatPlanning.exceptions.WorkerNotFoundException;
import fr.cesi.seatPlanning.model.Area;
import fr.cesi.seatPlanning.model.Organisation;
import fr.cesi.seatPlanning.model.Worker;
import fr.cesi.seatPlanning.repository.AreaRepository;
import fr.cesi.seatPlanning.repository.OrganisationRepository;
import fr.cesi.seatPlanning.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class OrganisationService {

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    WorkerRepository workerRepository;

    @Autowired
    AreaRepository areaRepository;

    public IOrganisationDTO getById(Integer id) throws OrganisationNotFoundException {
        return new OrganisationDTO(organisationRepository.findById(id).orElseThrow(OrganisationNotFoundException::new));
    }

    public IOrganisationDTO getByName(String name) throws OrganisationNotFoundException {
        return new OrganisationDTO(organisationRepository.findByName(name).orElseThrow(OrganisationNotFoundException::new));
    }

    public List<IOrganisationDTO> getAll(){
        List<Organisation> organisationsList = organisationRepository.findAll();
        List<IOrganisationDTO> organisationsDtoList = new ArrayList<>();
        for (Organisation organisation : organisationsList){
            organisationsDtoList.add(new OrganisationDTO(organisation));
        }
        return organisationsDtoList;
    }


    public IOrganisationDTO add(IOrganisationDTO organisationDTO) throws OrganisationAlreadyExistingException {
        try {
            this.getByName(organisationDTO.getName());
            throw new OrganisationAlreadyExistingException(organisationDTO.getName());
        } catch(OrganisationNotFoundException e){
            return new OrganisationDTO(organisationRepository.save(new Organisation(organisationDTO)));
        }
    }

    public IOrganisationDTO addWorker(Integer organisationId, Integer workerId) throws OrganisationNotFoundException, WorkerNotFoundException {
        Organisation organisation = organisationRepository.findById(organisationId).orElseThrow(OrganisationNotFoundException::new);
        Worker worker = workerRepository.findById(workerId).orElseThrow(WorkerNotFoundException::new);
        worker.setOrganisation(organisation);
        workerRepository.save(worker);
        return new OrganisationDTO(organisationRepository.findById(organisation.getId()).orElseThrow(OrganisationNotFoundException::new));
    }

    public IOrganisationDTO addArea (Integer organisationId, Integer areaId) throws OrganisationNotFoundException, AreaNotFoundException {
        Organisation organisation = organisationRepository.findById(organisationId).orElseThrow(OrganisationNotFoundException::new);
        Area area = areaRepository.findById(areaId).orElseThrow(AreaNotFoundException::new);
        area.setOrganisation(organisation);
        areaRepository.save(area);
        return new OrganisationDTO(organisationRepository.findById(organisation.getId()).orElseThrow(OrganisationNotFoundException::new));
    }
}
