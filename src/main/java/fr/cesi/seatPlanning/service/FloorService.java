package fr.cesi.seatPlanning.service;

import fr.cesi.seatPlanning.dto.FloorDTO;
import fr.cesi.seatPlanning.exceptions.AreaNotFoundException;
import fr.cesi.seatPlanning.exceptions.FloorNotFoundException;
import fr.cesi.seatPlanning.exceptions.TooBigAreaException;
import fr.cesi.seatPlanning.model.Area;
import fr.cesi.seatPlanning.model.Floor;
import fr.cesi.seatPlanning.repository.AreaRepository;
import fr.cesi.seatPlanning.repository.FloorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FloorService {

    @Autowired
    FloorRepository floorRepository;

    @Autowired
    AreaRepository areaRepository;

    public List<FloorDTO> getAll() {
        List<Floor> floorList = floorRepository.findAll();
        List<FloorDTO> floorDTOList = new ArrayList<>();
        for (Floor floor : floorList) {
            floorDTOList.add(new FloorDTO(floor));
        }
        return floorDTOList;
    }

    public FloorDTO add(FloorDTO floorDTO) {
        Floor floorToCreate = new Floor(floorDTO);
        Floor floorCreated = floorRepository.save(floorToCreate);
        FloorDTO floorReturn = new FloorDTO(floorCreated);
        return floorReturn;
    }

    public FloorDTO addArea(Integer floorId, Integer areaId) throws FloorNotFoundException, AreaNotFoundException, TooBigAreaException {
        Floor floor = floorRepository.findById(floorId).orElseThrow(FloorNotFoundException::new);
        Area area = areaRepository.findById(areaId).orElseThrow(AreaNotFoundException::new);
        Integer occupiedSurface = 0;
        for (Area _area : floor.getAreas()) {
            occupiedSurface += _area.getSurface();
        }
        if ( area.getSurface()> floor.getSurface() - occupiedSurface){
            throw new TooBigAreaException(floor.getSurface() - occupiedSurface, floor.getSurface());
        } else {
            area.setFloor(floor);
            areaRepository.save(area);
            return new FloorDTO(floorRepository.findById(floor.getId()).orElseThrow(FloorNotFoundException::new));
        }

    }
}
