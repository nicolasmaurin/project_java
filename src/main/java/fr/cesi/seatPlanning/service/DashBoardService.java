package fr.cesi.seatPlanning.service;

import fr.cesi.seatPlanning.dto.AreaDTO;
import fr.cesi.seatPlanning.dto.FloorDTO;
import fr.cesi.seatPlanning.dto.IOrganisationDTO;
import fr.cesi.seatPlanning.dto.OrganisationAreaOccupationDTO;

import fr.cesi.seatPlanning.exceptions.OrganisationNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class DashBoardService {
    class BatimentResume {

        Integer occupiedSurface = 0;
        private Map<Integer, OrganisationAreaOccupationDTO> orgaResume = new HashMap<>();

        BatimentResume(List<FloorDTO> floorList) {
            for (FloorDTO floor : floorList) {
                for (AreaDTO area : floor.getAreaDTOList()) {
                    if (orgaResume.get(area.getOrganisationId()) == null) {
                        orgaResume.put(area.getOrganisationId(), new OrganisationAreaOccupationDTO());
                    }
                    orgaResume.get(area.getOrganisationId()).addArea(area);
                    occupiedSurface += area.getSurface();
                }
            }
        }

        public  Map<Integer, OrganisationAreaOccupationDTO> getOrgaResume(){
            return this.orgaResume;
        }
    }

    @Autowired
    OrganisationService organisationService;

    @Autowired
    FloorService floorService;

    @Autowired
    AreaService areaService;

    public Map<String, OrganisationAreaOccupationDTO> getDashBoard() throws OrganisationNotFoundException {
        List<FloorDTO> floorDTOList = floorService.getAll();
        BatimentResume  batimentResume = new BatimentResume(floorDTOList);

        Map<Integer, OrganisationAreaOccupationDTO> orgaResume = batimentResume.getOrgaResume();
        Map<String, OrganisationAreaOccupationDTO> finalOrgaResume = new HashMap<>();
        String UNASSIGNED = "UNASSIGNED";
        for (Map.Entry<Integer, OrganisationAreaOccupationDTO> entry : orgaResume.entrySet()) {
            if (entry.getKey() == null) {
                entry.getValue().setOrganisationName(UNASSIGNED);
                finalOrgaResume.put(UNASSIGNED, entry.getValue());
            } else {
               IOrganisationDTO organisation = organisationService.getById(entry.getKey());
                entry.getValue().setOrganisationName(organisation.getName());
                finalOrgaResume.put(organisation.getName(), entry.getValue());
            }
        }
        return finalOrgaResume;
    }
}
