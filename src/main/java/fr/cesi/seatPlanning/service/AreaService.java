package fr.cesi.seatPlanning.service;

import fr.cesi.seatPlanning.dto.AreaDTO;
import fr.cesi.seatPlanning.exceptions.AreaNotFoundException;
import fr.cesi.seatPlanning.exceptions.FurnitureNotFoundException;
import fr.cesi.seatPlanning.exceptions.WorkerNotFoundException;
import fr.cesi.seatPlanning.model.Area;
import fr.cesi.seatPlanning.model.Furniture;
import fr.cesi.seatPlanning.model.Worker;
import fr.cesi.seatPlanning.repository.AreaRepository;
import fr.cesi.seatPlanning.repository.FloorRepository;
import fr.cesi.seatPlanning.repository.FurnitureRepository;
import fr.cesi.seatPlanning.repository.OrganisationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AreaService {

    @Autowired
    AreaRepository areaRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    FloorRepository floorRepository;

    @Autowired
    FurnitureRepository furnitureRepository;

    public List<AreaDTO> getAll() {
        List<Area> areaList = areaRepository.findAll();
        List<AreaDTO> areaDTOList = new ArrayList<>();
        for (Area area : areaList) {
            areaDTOList.add(new AreaDTO(area));
        }
        return areaDTOList;
    }

    public AreaDTO add(AreaDTO areaDTO){
        Area areaToCreate = new Area(areaDTO);
        Area areaCreated = areaRepository.save(areaToCreate);
        AreaDTO areaReturn = new AreaDTO(areaCreated);
        return areaReturn;
    }

    public AreaDTO addFurniture(Integer AreaId, Integer furnitureId) throws AreaNotFoundException, FurnitureNotFoundException {
        Area Area = areaRepository.findById(AreaId).orElseThrow(AreaNotFoundException::new);
        Furniture furniture = furnitureRepository.findById(furnitureId).orElseThrow(FurnitureNotFoundException::new);
        furniture.setArea(Area);
        furnitureRepository.save(furniture);
        return new AreaDTO(areaRepository.findById(Area.getId()).orElseThrow(AreaNotFoundException::new));
    }
}
