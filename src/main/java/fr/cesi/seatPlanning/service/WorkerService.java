package fr.cesi.seatPlanning.service;


import fr.cesi.seatPlanning.dto.WorkerDTO;
import fr.cesi.seatPlanning.exceptions.UserNotFoundException;
import fr.cesi.seatPlanning.exceptions.WorkerNotFoundException;
import fr.cesi.seatPlanning.exceptions.FurnitureNotFoundException;
import fr.cesi.seatPlanning.model.*;
import fr.cesi.seatPlanning.repository.FurnitureRepository;
import fr.cesi.seatPlanning.exceptions.OrganisationNotFoundException;
import fr.cesi.seatPlanning.model.Organisation;
import fr.cesi.seatPlanning.model.Worker;
import fr.cesi.seatPlanning.repository.OrganisationRepository;
import fr.cesi.seatPlanning.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WorkerService {

    @Autowired
    WorkerRepository workerRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    FurnitureRepository furnitureRepository;

    public List<WorkerDTO> getAll() {
        List<Worker> workerList = workerRepository.findAll();
        List<WorkerDTO> workerDtoList = new ArrayList<>();
        for (Worker worker : workerList) {
            workerDtoList.add(new WorkerDTO(worker));
        }
        return workerDtoList;
    }

    public WorkerDTO add(WorkerDTO workerDTO) {
        Worker workerToCreate = new Worker(workerDTO);
        Worker workerCreated = workerRepository.save(workerToCreate);
        WorkerDTO workerReturn = new WorkerDTO(workerCreated);
        return workerReturn;
    }


    public boolean delete(Integer workerId) throws WorkerNotFoundException {
        List<Furniture> furnituresList = furnitureRepository.findAllByWorkerId(workerId);
        System.out.println(furnituresList.size());
        for (Furniture furniture : furnituresList) {
            furniture.setWorker(null);
            furnitureRepository.save(furniture);
        }
        List<Furniture> furnituresList2 = furnitureRepository.findAllByWorkerId(workerId);
        System.out.println(furnituresList2.size());


        Worker workerModel = workerRepository.findById(workerId).orElseThrow(WorkerNotFoundException::new);
        workerModel.setOrganisation(null);
        workerRepository.save(workerModel);

        workerRepository.delete(workerModel);

        try {
            System.out.println(workerRepository.findById(workerId).orElseThrow(UserNotFoundException::new));
        } catch (UserNotFoundException e) {
            return true;
        }
        return false;
    }


    public List<WorkerDTO> getAllByOrganisationId(Integer organisationId) throws OrganisationNotFoundException {
        Organisation organisation = organisationRepository.findById(organisationId).orElseThrow(OrganisationNotFoundException::new);
        List<Worker> workerList = workerRepository.findAllByOrganisation(organisation);
        List<WorkerDTO> workerDtoList = new ArrayList<>();
        for (Worker worker : workerList) {
            workerDtoList.add(new WorkerDTO(worker));
        }
        return workerDtoList;
    }


    public WorkerDTO addFurniture(Integer workerId, Integer furnitureId) throws WorkerNotFoundException, FurnitureNotFoundException {
        Worker worker = workerRepository.findById(workerId).orElseThrow(WorkerNotFoundException::new);
        Furniture furniture = furnitureRepository.findById(furnitureId).orElseThrow(FurnitureNotFoundException::new);
        furniture.setWorker(worker);
        furnitureRepository.save(furniture);
        return new WorkerDTO(workerRepository.findById(worker.getId()).orElseThrow(WorkerNotFoundException::new));
    }
}
