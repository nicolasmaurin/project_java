package fr.cesi.seatPlanning.exceptions;

import fr.cesi.seatPlanning.FurnitureTypeEnum;

public class FurnitureTypeNotValidException extends BusinessException {
    public FurnitureTypeNotValidException() {
        String allowedFurnituresTypes = "";
        for (FurnitureTypeEnum c : FurnitureTypeEnum.values()) {
            allowedFurnituresTypes += c.name() + ", ";
        }
        this.message = "type of furniture is not valid. Allowed types are : " + allowedFurnituresTypes;
    }
}
