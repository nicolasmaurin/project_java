package fr.cesi.seatPlanning.exceptions;

public class AreaNotFoundException extends BusinessException {
    public AreaNotFoundException(){
        this.message = "Area not found for given id";
    }
}
