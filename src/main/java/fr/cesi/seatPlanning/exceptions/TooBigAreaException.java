package fr.cesi.seatPlanning.exceptions;

public class TooBigAreaException extends BusinessException {
    public TooBigAreaException(Integer availableSurface,Integer maxSurface){
        this.message = "This area can't fit on this floor. " + String.format("%s m² are available on %s m²", availableSurface, maxSurface);
    }
}
