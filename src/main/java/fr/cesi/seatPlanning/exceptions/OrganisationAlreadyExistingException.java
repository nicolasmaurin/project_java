package fr.cesi.seatPlanning.exceptions;

public class OrganisationAlreadyExistingException extends BusinessException {
    public OrganisationAlreadyExistingException(String name){
        this.message = String.format("Organisation %s already exists", name);
    }
}
