package fr.cesi.seatPlanning.exceptions;

public class FloorNotFoundException extends BusinessException {
    public FloorNotFoundException(){
        this.message = "Floor not found for given id";
    }
}
