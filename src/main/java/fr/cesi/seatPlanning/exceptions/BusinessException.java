package fr.cesi.seatPlanning.exceptions;

public class BusinessException extends Exception {
    protected String message;
    @Override
    public String getMessage() {
        return this.message;
    }
}
