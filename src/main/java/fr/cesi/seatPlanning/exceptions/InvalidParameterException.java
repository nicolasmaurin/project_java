package fr.cesi.seatPlanning.exceptions;

public class InvalidParameterException extends Exception {

    public InvalidParameterException(String message) {
        super(message);
    }
}
