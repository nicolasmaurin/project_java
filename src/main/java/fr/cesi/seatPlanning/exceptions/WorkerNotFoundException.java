package fr.cesi.seatPlanning.exceptions;

public class WorkerNotFoundException extends BusinessException{
    public WorkerNotFoundException(){
        this.message = "Worker not found for given id";
    }
}
