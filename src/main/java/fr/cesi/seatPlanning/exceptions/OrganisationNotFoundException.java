package fr.cesi.seatPlanning.exceptions;

public class OrganisationNotFoundException extends BusinessException {
    public OrganisationNotFoundException(){
        this.message = "Organisation not found for given id";
    }
}
