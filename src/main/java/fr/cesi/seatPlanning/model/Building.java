package fr.cesi.seatPlanning.model;

import fr.cesi.seatPlanning.dto.BuildingDTO;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Building {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private Integer maxFloors;

    @OneToMany(mappedBy = "building", fetch = FetchType.EAGER)
    private Set<Floor> floors;

    public Building() {
    }

    public Building(BuildingDTO buildingDTO) {
        this.id = buildingDTO.getId();
        this.name = buildingDTO.getName();
        this.maxFloors = buildingDTO.getMaxFloors();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxFloors() {
        return maxFloors;
    }

    public void setMaxFloors(Integer maxFloors) {
        this.maxFloors = maxFloors;
    }

    public Set<Floor> getFloors() {
        return floors;
    }

    public void setFloors(Set<Floor> floors) {
        this.floors = floors;
    }
}
