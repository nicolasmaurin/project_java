package fr.cesi.seatPlanning.model;

import fr.cesi.seatPlanning.dto.IOrganisationDTO;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Organisation {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)

    private Integer id;

    private String name;

    @OneToMany(mappedBy="organisation", fetch = FetchType.EAGER, orphanRemoval=true, cascade = CascadeType.ALL)
    private Set<Worker> workers;

    @OneToMany(mappedBy = "organisation", fetch = FetchType.EAGER)
    private Set<Area> areas;

    public Organisation() {
        //pouet
    }


    public Organisation(IOrganisationDTO organisation) {
        name = organisation.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(Set<Worker> workers) {
        this.workers = workers;
    }

    public Set<Area> getAreas() {
        return areas;
    }

    public void setAreas(Set<Area> areas) {
        this.areas = areas;
    }
}
