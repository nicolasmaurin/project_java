package fr.cesi.seatPlanning.model;

import fr.cesi.seatPlanning.dto.AreaDTO;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Area {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private Integer surface;

    @ManyToOne
    @JoinColumn(name = "organisation_id", nullable = true)
    private Organisation organisation;

    @ManyToOne
    @JoinColumn(name = "floor_id", nullable = true)
    private Floor floor;

    @OneToMany(mappedBy = "area", fetch = FetchType.EAGER, orphanRemoval=true)
    private Set<Furniture> furnitures;

    public Area() {
    }

    public Area(AreaDTO areaDTO) {
        this.name = areaDTO.getName();
        this.surface = areaDTO.getSurface();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSurface() {
        return surface;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public Floor getFloor() {
        return floor;
    }

    public void setFloor(Floor floor) {
        this.floor = floor;
    }

    public Set<Furniture> getFurnitures() {
        return furnitures;
    }

    public void setFurnitures(Set<Furniture> furnitures) {
        this.furnitures = furnitures;
    }
}
