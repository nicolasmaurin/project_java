package fr.cesi.seatPlanning.model;
import fr.cesi.seatPlanning.dto.WorkerDTO;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Worker {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Integer id;
    private String fullName;

    @ManyToOne
    @JoinColumn(name = "organisation_id", nullable = true)
    private Organisation organisation;

    @OneToMany(mappedBy = "worker", fetch = FetchType.EAGER)
    private Set<Furniture> furnitures;

    public Worker (){
    }

    public Worker (WorkerDTO workerDto){
        this.fullName = workerDto.getFullName();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public Set<Furniture> getFurnitures() {
        return furnitures;
    }

    public void setFurnitures(Set<Furniture> furnitures) {
        this.furnitures = furnitures;
    }
}
