package fr.cesi.seatPlanning.model;

import fr.cesi.seatPlanning.dto.FloorDTO;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Floor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private Integer number;
    private Integer surface;

    @OneToMany(mappedBy = "floor", fetch = FetchType.EAGER)
    private Set<Area> areas;

    @ManyToOne
    @JoinColumn(name = "building_id", nullable = true)
    private Building building;

    public Floor() {
    }

    public Floor(FloorDTO floorDTO) {
        this.id = floorDTO.getId();
        this.name = floorDTO.getName();
        this.number = floorDTO.getNumber();
        this.surface = floorDTO.getSurface();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getSurface() {
        return surface;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    public Set<Area> getAreas() {
        return areas;
    }

    public void setAreas(Set<Area> areas) {
        this.areas = areas;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }
}
