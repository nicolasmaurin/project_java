package fr.cesi.seatPlanning.model;

import fr.cesi.seatPlanning.dto.FurnitureDTO;

import javax.persistence.*;

@Entity
public class Furniture {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Integer id;
    private String type;
    private String title;

    @ManyToOne
    @JoinColumn(name = "area_id", nullable = true)
    private Area area;

    @ManyToOne
    @JoinColumn(name = "worker_id", nullable = true)
    private Worker worker;

    public Furniture() {
    }

    public Furniture(FurnitureDTO furnitureDTO){
        this.type = furnitureDTO.getType();
        this.title = furnitureDTO.getTitle();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }
}
