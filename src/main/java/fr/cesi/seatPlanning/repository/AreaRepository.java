package fr.cesi.seatPlanning.repository;

import fr.cesi.seatPlanning.model.Area;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AreaRepository extends CrudRepository<Area, Integer> {

    Optional<Area> findById(Integer id);
    Optional<Area> findByName(String Name);
    List<Area> findAll();
}
