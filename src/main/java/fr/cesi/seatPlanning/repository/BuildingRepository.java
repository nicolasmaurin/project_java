package fr.cesi.seatPlanning.repository;

import fr.cesi.seatPlanning.model.Building;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BuildingRepository extends CrudRepository<Building, Integer> {

    Optional<Building> findById(Integer id);

    Optional<Building> findByName(String Name);

    List<Building> findAll();
}
