package fr.cesi.seatPlanning.repository;

import fr.cesi.seatPlanning.model.Furniture;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FurnitureRepository extends CrudRepository<Furniture, Integer> {

    Optional<Furniture> findById(Integer id);
    Optional<Furniture> findByType(String type);
    List<Furniture> findAll();
    List<Furniture> findAllByWorkerId(Integer workerId);

}
