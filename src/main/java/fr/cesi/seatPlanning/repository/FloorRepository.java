package fr.cesi.seatPlanning.repository;

import fr.cesi.seatPlanning.model.Floor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FloorRepository extends CrudRepository<Floor, Integer> {

    Optional<Floor> findById(Integer id);

    Optional<Floor> findByName(String Name);

    List<Floor> findAll();
}
