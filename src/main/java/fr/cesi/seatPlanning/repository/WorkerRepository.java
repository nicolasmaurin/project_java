package fr.cesi.seatPlanning.repository;

import fr.cesi.seatPlanning.model.Organisation;
import fr.cesi.seatPlanning.model.Worker;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface WorkerRepository extends CrudRepository<Worker, Integer> {

    Optional<Worker> findById(Integer id);
    Optional<Worker> findByFullName(String fullName);
    List<Worker> findAll();
    List<Worker> findAllByOrganisation(Organisation organisation);

}
