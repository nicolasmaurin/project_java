package fr.cesi.seatPlanning.repository;

import fr.cesi.seatPlanning.model.Organisation;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface OrganisationRepository extends CrudRepository<Organisation, Integer> {

    Optional<Organisation> findById(Integer id);
    Optional<Organisation> findByName(String name);
    List<Organisation> findAll();
}
