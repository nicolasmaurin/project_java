package fr.cesi.seatPlanning.dto;

import java.util.ArrayList;
import java.util.List;

public class OrganisationAreaOccupationDTO {
    private List<AreaDTO> areaList;
    private Integer surface = 0;
    private String organisationName = "NC";

    public OrganisationAreaOccupationDTO(){
        this.areaList = new ArrayList<>();
    }

    public void addSurface(Integer surface) {
        this.surface += surface;
    }

    public Integer getSurface() {
        return surface;
    }

    public void addArea(AreaDTO area){
        this.areaList.add(area);
        this.addSurface(area.getSurface());
    }

    public List<AreaDTO> getAreaList(){
        return this.areaList;
    }

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }
}
