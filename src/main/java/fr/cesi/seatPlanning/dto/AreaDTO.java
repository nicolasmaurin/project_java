package fr.cesi.seatPlanning.dto;

import fr.cesi.seatPlanning.model.Area;
import fr.cesi.seatPlanning.model.Furniture;

import java.util.HashSet;
import java.util.Set;

import fr.cesi.seatPlanning.tools.DTOValidator;

import java.security.InvalidParameterException;

public class AreaDTO {

    private Integer id;
    private String name;
    private Integer surface;
    private Set<FurnitureDTO> furnitures;
    private Integer organisationId;
    private Integer floorId;

    public AreaDTO() {
    }

    public AreaDTO(Area area) {
        id = area.getId();
        name = area.getName();
        surface = area.getSurface();
        Set<Furniture> furnituresModelList = area.getFurnitures();
        Set<FurnitureDTO> furnituresDTOList = new HashSet<>();
        if (furnituresModelList != null) {
            for (Furniture furniture : furnituresModelList) {
                furnituresDTOList.add(new FurnitureDTO(furniture));
            }
        }
        this.furnitures = furnituresDTOList;

        if (area.getOrganisation() != null) {
            organisationId = area.getOrganisation().getId();
        }
        if (area.getFloor() != null) {
            this.floorId = area.getFloor().getId();
        }

    }

    public void validate() throws InvalidParameterException {
        DTOValidator.validateNonNullNonEmpty("surface", this.surface.toString());
        DTOValidator.validatePositiveInt("surface", this.surface);
        DTOValidator.validateNonNullNonEmpty("name", this.name);

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSurface() {
        return surface;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    public Set<FurnitureDTO> getFurnitures() {
        return furnitures;
    }

    public void setFurnitures(Set<FurnitureDTO> furnitures) {
        this.furnitures = furnitures;
    }

    public Integer getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        this.organisationId = organisationId;
    }

    public Integer getFloorId() {
        return floorId;
    }

    public void setFloorId(Integer floorId) {
        this.floorId = floorId;
    }
}
