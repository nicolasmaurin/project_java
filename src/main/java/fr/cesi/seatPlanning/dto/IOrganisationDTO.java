package fr.cesi.seatPlanning.dto;

import java.util.Set;

public interface IOrganisationDTO {

    void validate();
    String getName();
    void setName(String name);
    Integer getId();
    void setId(Integer id);
    Set<WorkerDTO> getWorkers();
    void setWorkers(Set<WorkerDTO> workers);
    Set<AreaDTO> getAreas();
    void setAreas(Set<AreaDTO> areas);
}
