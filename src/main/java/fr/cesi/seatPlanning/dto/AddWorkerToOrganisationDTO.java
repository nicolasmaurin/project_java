package fr.cesi.seatPlanning.dto;

public class AddWorkerToOrganisationDTO {
    public Integer getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        this.organisationId = organisationId;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }

    Integer organisationId;
    Integer workerId;
    AddWorkerToOrganisationDTO(){

    }
}
