package fr.cesi.seatPlanning.dto;

import fr.cesi.seatPlanning.tools.DTOValidator;

import java.security.InvalidParameterException;

public class AddFurnitureToAreaDTO {

    private Integer furnitureId;
    private Integer areaId;

    public AddFurnitureToAreaDTO() {
    }

    public void validate() {
        try {
            DTOValidator.validateNonNullNonEmpty("furnitureId", this.furnitureId.toString());
            DTOValidator.validateNonNullNonEmpty("areaId", this.areaId.toString());
            DTOValidator.validatePositiveInt("furnitureId", this.furnitureId);
            DTOValidator.validatePositiveInt("areaId", this.areaId);
        } catch (NullPointerException e) {
            throw new InvalidParameterException(" furnitureId and areaId cannot be null");
        }
    }

    public Integer getFurnitureId() {
        return furnitureId;
    }

    public void setFurnitureId(Integer furnitureId) {
        this.furnitureId = furnitureId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }
}
