package fr.cesi.seatPlanning.dto;

import fr.cesi.seatPlanning.model.Building;
import fr.cesi.seatPlanning.model.Floor;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

public class BuildingDTO {

    private Integer id;
    private String name;
    private Integer maxFloors;

    public List<FloorDTO> getFloors() {
        return floors;
    }

    public void setFloors(List<FloorDTO> floors) {
        this.floors = floors;
    }

    List<FloorDTO> floors;

    public BuildingDTO() {

    }

    public BuildingDTO(Building building) {
        this.id = building.getId();
        this.name = building.getName();
        this.maxFloors = building.getMaxFloors();
        List<FloorDTO >floorsList = new ArrayList<>();
        if (!isNull(building.getFloors())){
            for (Floor floor : building.getFloors()){
                floorsList.add(new FloorDTO(floor));
            }
        }
        this.floors = floorsList;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxFloors() {
        return maxFloors;
    }

    public void setMaxFloors(Integer maxFloors) {
        this.maxFloors = maxFloors;
    }
}
