package fr.cesi.seatPlanning.dto;

public class AddAreaToFloorDTO {
    Integer areaId;
    Integer floorId;

    public AddAreaToFloorDTO(){
    }

    public Integer getFloorId() {
        return floorId;
    }

    public void setFloorId(Integer floorId) {
        this.floorId = floorId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }
}
