package fr.cesi.seatPlanning.dto;

public class AddAreaToOrganisationDTO {
    Integer organisationId;
    Integer areaId;

    public AddAreaToOrganisationDTO() {
    }

    public Integer getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        this.organisationId = organisationId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

}
