package fr.cesi.seatPlanning.dto;

import fr.cesi.seatPlanning.model.Furniture;
import fr.cesi.seatPlanning.model.Worker;

import java.util.HashSet;
import java.util.Set;

public class WorkerDTO {

    private Integer id;
    private String fullName;
    private Set<FurnitureDTO> furnitures;

    public WorkerDTO(){
    }

    public WorkerDTO(Worker worker){
        id = worker.getId();
        fullName = worker.getFullName();
        Set<Furniture> furnituresModelList = worker.getFurnitures();
        Set<FurnitureDTO> furnituresDTOList = new HashSet<>();
        if (furnituresModelList != null ){
            for (Furniture furniture : furnituresModelList){
                furnituresDTOList.add(new FurnitureDTO(furniture));
            }
        }
        this.furnitures = furnituresDTOList;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Set<FurnitureDTO> getFurnitures() {
        return furnitures;
    }

    public void setFurnitures(Set<FurnitureDTO> furnitures) {
        this.furnitures = furnitures;
    }
}
