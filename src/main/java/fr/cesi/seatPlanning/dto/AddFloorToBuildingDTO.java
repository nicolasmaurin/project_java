package fr.cesi.seatPlanning.dto;

public class AddFloorToBuildingDTO {

    Integer buildingId;
    Integer floorId;

    public AddFloorToBuildingDTO() {
    }

    public Integer getFloorId() {
        return floorId;
    }

    public void setFloorId(Integer floorId) {
        this.floorId = floorId;
    }

    public Integer getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Integer buildingId) {
        this.buildingId = buildingId;
    }
}
