package fr.cesi.seatPlanning.dto;

import fr.cesi.seatPlanning.model.Area;
import fr.cesi.seatPlanning.model.Floor;
import fr.cesi.seatPlanning.tools.DTOValidator;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class FloorDTO {

    private Integer id;
    private String name;
    private Integer number;
    private Integer surface;
    private List<AreaDTO> areaDTOList;
    public FloorDTO() {
    }

    public FloorDTO(Floor floor) {
        id = floor.getId();
        name = floor.getName();
        number = floor.getNumber();
        surface = floor.getSurface();
        List<AreaDTO> areaList = new ArrayList<>();
        if (floor.getAreas() != null) {
            for (Area area : floor.getAreas()) {
                areaList.add(new AreaDTO(area));
            }
        }
        this.areaDTOList = areaList;
    }

    public void validate() throws InvalidParameterException {
        DTOValidator.validateNonNullNonEmpty("surface", this.surface.toString());
        DTOValidator.validatePositiveInt("surface", this.surface);
        DTOValidator.validateNonNullNonEmpty("name", this.name);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getSurface() {
        return surface;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    public List<AreaDTO> getAreaDTOList() {
        return areaDTOList;
    }

    public void setAreaDTOList(List<AreaDTO> areaDTOList) {
        this.areaDTOList = areaDTOList;
    }
}
