package fr.cesi.seatPlanning.dto;

import fr.cesi.seatPlanning.FurnitureTypeEnum;
import fr.cesi.seatPlanning.exceptions.FurnitureTypeNotValidException;
import fr.cesi.seatPlanning.model.Furniture;
import fr.cesi.seatPlanning.tools.DTOValidator;

import java.awt.*;

import static java.util.Objects.isNull;

public class FurnitureDTO {

    private Integer id;
    private String type;
    private String title;
    private Integer workerId;
    private Integer areaId;

    public FurnitureDTO() {
    }

    public FurnitureDTO(Furniture furniture) {
        id = furniture.getId();
        type = furniture.getType();
        title = furniture.getTitle();
        workerId = isNull(furniture.getWorker()) ? null : furniture.getWorker().getId();
        areaId = isNull(furniture.getArea()) ? null : furniture.getArea().getId();
    }

    public void validate() throws FurnitureTypeNotValidException {
        DTOValidator.validateNonNullNonEmpty("title",this.title);
        DTOValidator.validateNonNullNonEmpty("type",this.type);

        // TODO : Generify and Export in validator
        Integer counter = 1;

        for (FurnitureTypeEnum c : FurnitureTypeEnum.values()) {
            if (c.name().equals(this.type)) {
                break;
            }
            if(counter == FurnitureTypeEnum.values().length){
                throw new FurnitureTypeNotValidException();
            }
            counter++;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }
}
