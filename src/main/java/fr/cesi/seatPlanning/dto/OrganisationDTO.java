package fr.cesi.seatPlanning.dto;

import fr.cesi.seatPlanning.model.Area;
import fr.cesi.seatPlanning.model.Organisation;
import fr.cesi.seatPlanning.model.Worker;
import fr.cesi.seatPlanning.tools.DTOValidator;

import java.util.HashSet;
import java.util.Set;

public class OrganisationDTO implements IOrganisationDTO{

    private Integer id;
    private String name;
    private Set<WorkerDTO> workers;
    private Set<AreaDTO> areas;

    public OrganisationDTO() {
    }

    public OrganisationDTO(Organisation organisation) {
        id = organisation.getId();
        name = organisation.getName();
        Set<Worker> workersModelList = organisation.getWorkers();
        Set<WorkerDTO> workersDTOList = new HashSet<>();
        if (workersModelList != null ){
            for (Worker worker : workersModelList){
                workersDTOList.add(new WorkerDTO(worker));
            }
        }
        this.workers = workersDTOList;

        Set<Area> areasModelList = organisation.getAreas();
        Set<AreaDTO> areasDTOList = new HashSet<>();
        if (areasModelList != null ){
            for (Area area : areasModelList){
                areasDTOList.add(new AreaDTO(area));
            }
        }
        this.areas = areasDTOList;
    }

    public void validate(){
        DTOValidator.validateNonNullNonEmpty("name", this.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<WorkerDTO> getWorkers() {
        return workers;
    }

    public void setWorkers(Set<WorkerDTO> workers) {
        this.workers = workers;
    }

    public Set<AreaDTO> getAreas() {
        return areas;
    }

    public void setAreas(Set<AreaDTO> areas) {
        this.areas = areas;
    }
}
