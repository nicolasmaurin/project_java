package fr.cesi.seatPlanning.dto;

import fr.cesi.seatPlanning.tools.DTOValidator;

import java.security.InvalidParameterException;

public class AddFurnitureToWorkerDTO {

    private Integer furnitureId;
    private Integer workerId;

    public AddFurnitureToWorkerDTO(){}

    public void validate(){

            try {
                DTOValidator.validateNonNullNonEmpty("furnitureId", this.furnitureId.toString());
                DTOValidator.validateNonNullNonEmpty("areaId", this.workerId.toString());
                DTOValidator.validatePositiveInt("furnitureId", this.furnitureId);
                DTOValidator.validatePositiveInt("areaId", this.workerId);
            } catch (NullPointerException e) {
                throw new InvalidParameterException(" furnitureId and areaId cannot be null");
            }

    }

    public Integer getFurnitureId() {
        return furnitureId;
    }

    public void setFurnitureId(Integer furnitureId) {
        this.furnitureId = furnitureId;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }
}
